// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

//Lazy Cache
int cache[1000000] = {0};

// ------------
// cycle_length
// ------------

int cycle_length(long int x) {
    assert(x >= 0);
    if(x == 0)
        return 0;
    if(x == 1)
        return 1;
    int length = 1;
    while(x > 1) {
        if(x < 1000000 && cache[x] > 0)   //Lazy cache implementation, manage overflow
            return cache[x] + length - 1;
        if(x % 2 == 0)
            x /= 2;
        else {         //Optimization based on class
            x = (3*x)/2 + 1;
            length += 1;
        }
        length += 1;
    }
    return length;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>
    if(i > j) {        //ensure i < j
        i += j;
        j = i - j;
        i -= j;
    }
    assert(j < 1000000);
    if(i < (j/2))
        i = j/2;
    int mcl = 0;
    int length = 0;
    for(long int x = i; x <= j; x++) {
        if(cache[x] > 0) //Lazy Cache implementation
            length = cache[x];
        else {
            length = cycle_length(x);
            cache[x] = length;
        }
        if(length > mcl)
            mcl = length;
    }
    return mcl;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
